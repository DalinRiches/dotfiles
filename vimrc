" Gotta be first to be IMproved
set nocompatible
filetype off

" --- General Settings ---
set ruler
set number relativenumber
set showcmd
set incsearch
set nohlsearch
set guifont=Fira\ Code:h12
set wrap!

syntax on

"--- Tab Settings ---
set tabstop=3
set softtabstop=0
set expandtab
set shiftwidth=3
set smarttab

call plug#begin('~/.vim/plugged')

Plug 'VundleVim/Vundle.vim'
Plug 'w0rp/ale'
Plug 'lervag/vimtex'
Plug 'ervandew/supertab'
Plug 'SirVer/ultisnips'
Plug 'justinmk/vim-syntax-extra'
Plug 'honza/vim-snippets'
Plug 'majutsushi/tagbar'
Plug 'danilo-augusto/vim-afterglow'

"--- Version Control Plugins
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

"--- Languge Plugins ---
Plug 'rust-lang/rust.vim'
Plug 'hdima/python-syntax'
Plug 'autozimu/LanguageClient-neovim', {
     \ 'branch': 'next',
     \ 'do': 'bash install.sh',
     \ }
Plug 'Shougo/deoplete.nvim'

"--- Effeciency Enhancement Plugins ---
Plug 'scrooloose/nerdcommenter'
Plug 'ludovicchabant/vim-gutentags'
Plug 'townk/vim-autoclose'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }

"--- Themeing Plugins ---
Plug 'itchyny/lightline.vim'
Plug 'junegunn/seoul256.vim'
Plug 'nanotech/jellybeans.vim'
Plug 'liuchengxu/space-vim-theme'
Plug 'jacoborus/tender.vim'
Plug 'nightsense/carbonized'
Plug 'NLKNguyen/papercolor-theme'
Plug 'jnurmine/Zenburn'
Plug 'morhetz/gruvbox'

" All of your Plugins must be added before the following line
call plug#end()

let $FZF_DEFAULT_COMMAND = 'ag -g ""'

let g:python3_host_prog = "/usr/bin/python3"
let g:python_host_prog = "/usr/bin/python2"

" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
    \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
    \ 'python': ['/usr/local/bin/pyls'],
    \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
    \ }

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" --- airblade/vim-gitgutter---
let g:gitgutter_enabled = 1
let g:gitgutter_override_sign_column_highlight = 0

" --- w0rp/ale ---
let g:ale_c_gcc_executable = 'arm-none-eabi-gcc'
let g:ale_c_parse_makefile = '1'

let g:deoplete#enable_at_startup = 1

" ---majutsushi/tagbar---
nmap <F8> :TagbarToggle<CR>

" ---SirVer/ultisnips---
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetDirectories= [$HOME.'/Documents/vundle_plugins/vim-snippets/UltiSnips']

" --- itchyny/lightline.vim ---
" Always show statusbar
set laststatus=2
let g:lightline = {
   \ 'colorscheme': 'wombat',
   \ }

" Make the middle sections background transparent
let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
let s:palette.inactive.middle = s:palette.normal.middle
let s:palette.tabline.middle = s:palette.normal.middle


" --- scrooloose/nerdcommenter ---
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 0
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
let g:NERDCommentEmptyLines = 1

"--- Keybindings ---
" Ctrl-j/k deletes blank line below/above, and Alt-j/k inserts.
nnoremap <silent> <leader>dj m`:silent +g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent> <leader>dk m`:silent -g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent> <leader>j :set paste<CR>m`o<Esc>``:set nopaste<CR>
nnoremap <silent> <leader>k :set paste<CR>m`O<Esc>``:set nopaste<CR>`
"-----------------------------------------------------------------------------
"----- Color Scheme Settings -----
"-----   - Its last as other plugins can mess this up
"------------------------------------------------------------------------------
" --- Paper Colour settings ---
let g:PaperColor_Theme_Options = {
  \   'theme': {
  \     'default': {
  \       'transparent_background': 1,
  \       'allow_bold': 1,
  \       'allow_italic': 0
  \     }
  \   },
  \   'language': {
  \     'python': {
  \       'highlight_builtins' : 1
  \     },
  \     'cpp': {
  \       'highlight_standard_library': 1
  \     },
  \     'c': {
  \       'highlight_builtins' : 1
  \     }
  \   }
  \ }

" --- Python Highlight Settings ---
let python_highlight_all = 1

" --- Set the colorscheme ---
colorscheme gruvbox

" --- Make Background Transparent
highlight Normal guibg=NONE ctermbg=NONE
highlight NonText guibg=NONE ctermbg=NONE
highlight SignColumn ctermbg=NONE
highlight LineNr ctermbg=NONE
highlight CursorLineNr ctermbg=NONE
set foldcolumn=2
highlight foldcolumn ctermbg=NONE
highlight VertSplit ctermbg=NONE ctermfg=NONE
highlight clear SignColumn
highlight GitGutterAdd ctermfg=green
highlight GitGutterChange ctermfg=yellow
highlight GitGutterDelete ctermfg=red
highlight GitGutterChangeDelete ctermfg=yellow
