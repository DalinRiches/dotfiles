# --- Set Tmux to use the appropriate TERM ---
set -g default-terminal "screen-256color"

###########################################################################
############# Bindings
###############################################################################

# Set the prefix to `ctrl + a` instead of `ctrl + b`
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# Use | and - to split a window vertically and horizontally instead of " and % respoectively
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
unbind '"'
unbind %

# Open ~/.tmux.conf in vim and reload settings on quit
unbind e
bind e new-window -n '~/.tmux.conf' "sh -c 'vim ~/.tmux.conf && tmux source ~/.tmux.conf'"

# Bind D to resize the window to be 8 lines smaller
bind D resize-pane -D 8

# Move around panes with hjkl, as one would in vim after pressing ctrl + w
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Switch betewen panes using alt + arrow --> NOT WORKING FOR LEFT AND RIGHT YET
#bind -n C-Left select-pane -L
#bind -n C-Right select-pane -R
#bind -n C-Up select-pane -U
#bind -n C-Down select-pane -D

# Use shift + arrow key to move between windows in a session
bind -n S-Left  previous-window
bind -n S-Right next-window

# Use r to quickly reload tmux settings
unbind r
bind r \
   source-file ~/.tmux.conf \;\
   display 'Reloaded tmux config'

# Use m to toggle mouse mode
unbind m
bind m setw mouse

# Use a to toggle synchronize panes
bind a set-window-option synchronize-panes

# prefix + / to search
bind-key / copy-mode \; send-key ?

# prefix + h to clear screen and history buffer
bind y \
    send-keys "C-c; clear && tmux clear-history" \;\
    send-keys "Enter" \;\
    display 'Screen cleared'
###############################################################################
############# Date/Time values that can be displayed in the status line
###############################################################################

# $(echo $USER) - shows the current username
# %a --> Day of week (Mon)
# %A --> Day of week Expanded (Monday)

# %b --> Month (Jan)
# %d --> Day (31)
# %Y --> Year (2017)

# %D --> Month/Day/Year (12/31/2017)
# %v --> Day-Month-Year (31-Dec-2017)

# %r --> Hour:Min:Sec AM/PM (12:30:27 PM)
# %T --> 24 Hour:Min:Sec (16:30:27)
# %X --> Hour:Min:Sec (12:30:27)
# %R --> 24 Hour:Min (16:30)
# %H --> 24 Hour (16)
# %l --> Hour (12)
# %M --> Mins (30)
# %S --> Seconds (09)
# %p --> AM/PM (AM)

# For a more complete list view: https://linux.die.net/man/3/strftime

###############################################################################
#  Style Definitions
###############################################################################
set -g status-style fg=white,bg=default,bright
set -g status-left-style fg=green
set -g status-right-style fg=blue,bright

set -g pane-border-style fg=white,bg=default,dim
set -g pane-active-border-style fg=white,bg=default,bright

set -g window-status-style fg=white,bg=default,dim
set -g window-status-current-style fg=white,bg=default,bright

set -g message-style fg=green,bg=default,bright
set -g message-command-style fg=white,bright

###############################################################################
############# Settings
###############################################################################

# Refresh status line every 5 seconds
set -g status-interval 1
set -g status-justify centre
set -g status-left ' #H '
set -g status-right ' %b %d %l:%M %p '

set -g window-status-current-format " > #I:#W #{?window_zoomed_flag,🔥  ,}< "
set -g window-status-format " #I:#W "

# Start window and pane indices at 2.
set -g base-index 1
set -g pane-base-index 1

set-option -sg escape-time 10
#D ()
#F ()
#H (hostname)
#I (window index)
#P ()
#S (session index)
#T (pane title)
#W (currnet task like vim if editing a file in vim or zsh if running zsh)

# Set the history limit so we get lots of scrollback.
setw -g history-limit 50000
